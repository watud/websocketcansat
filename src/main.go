package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"net"

	"github.com/gorilla/websocket"
)

const (
    CONN_HOST = "192.168.43.95"
    CONN_PORT = "80"
    CONN_TYPE = "tcp"
)

// Info Estructura de información
type Info struct {
	Hora           string
	Latitud        float32
	NorteSur       byte
	Longitud       float32
	EsteOeste      byte
	SateliteUsados int
	Altitud        int
	Presion        int
	AceleracionX   string
	AceleracionY   string
	AceleracionZ   string
	VelocidadX     string
	VelocidadY     string
	VelocidadZ     string
	CampoX         string
	CampoY         string
	CampoZ         string
	Temperatura    string
	Humedad        int
	Vibraciones    int
}

var addr = flag.String("addr", ":"+os.Getenv("PORT"), "http service address")
var upgrader = websocket.Upgrader{
	ReadBufferSize:  0,
	WriteBufferSize: 1024,
}

func main() {
	flag.Parse()
	log.SetFlags(0)
	http.HandleFunc("/ws", handleConnections)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		body, _ := ioutil.ReadFile("./views/index.html")
		w.Write(body)
	})

	log.Println("Server escuchando en http://127.0.0.1:" + os.Getenv("PORT"))
	log.Fatal(http.ListenAndServe(*addr, nil))
}

func handleConnections(w http.ResponseWriter, r *http.Request) {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	conn, err := upgrader.Upgrade(w, r, nil)
	checkErr(err, "INICIA UN NUEVO WEBSOCKET")

	defer conn.Close()

	// Connect
	tcpAddr, err := net.ResolveTCPAddr("tcp4", CONN_HOST+":"+CONN_PORT)
    connTCP, err := net.DialTCP("tcp", nil, tcpAddr)
    if err != nil {
        log.Println("Dial failed:", err.Error())
        os.Exit(1)
	}

	for {
		// t := time.Now()
		// msg := Info{
		// 	Hora:           fmt.Sprintf("%02d%02d%02d", t.Hour(), t.Minute(), t.Second()),
		// 	Latitud:        rand.Float32() * float32(10000),
		// 	NorteSur:       randLetter([]byte{'N', 'S'}),
		// 	Longitud:       rand.Float32() * float32(10000),
		// 	EsteOeste:      randLetter([]byte{'E', 'W'}),
		// 	SateliteUsados: rand.Intn(12),
		// 	Altitud:        rand.Intn(10000),
		// 	Presion:        rand.Intn(1000),
		// 	AceleracionX:   randNumWithSign(1, 4),
		// 	AceleracionY:   randNumWithSign(1, 4),
		// 	AceleracionZ:   randNumWithSign(1, 4),
		// 	VelocidadX:     randNumWithSign(3, 0),
		// 	VelocidadY:     randNumWithSign(3, 0),
		// 	VelocidadZ:     randNumWithSign(3, 0),
		// 	CampoX:         randNumWithSign(3, 0),
		// 	CampoY:         randNumWithSign(3, 0),
		// 	CampoZ:         randNumWithSign(3, 0),
		// 	Temperatura:    randNumWithSign(2, 0),
		// 	Humedad:        rand.Intn(100),
		// 	Vibraciones:    rand.Intn(1000),
		// }

		reply := make([]byte, 1024)

		_, err = connTCP.Read(reply)
		if err != nil {
			log.Println("Read to server failed:", err.Error())
			os.Exit(1)
		}

		log.Println("Datos recibido de "+CONN_HOST+":"+CONN_PORT, string(reply))
		err := conn.WriteMessage(1, reply)
		checkErr(err, "SE ENVIÁ EL MENSAJE A LOS CLIENTES")
		// time.Sleep(1 * time.Second)
	}

	conn.Close()
}

func (i Info) toArrayBytes() []byte {
	return []byte(fmt.Sprintf("%s,%f,%s,%f,%s,%d,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d;",
		i.Hora,
		i.Latitud,
		string(i.NorteSur),
		i.Longitud,
		string(i.EsteOeste),
		i.SateliteUsados,
		i.Altitud,
		i.Presion,
		i.AceleracionX,
		i.AceleracionY,
		i.AceleracionZ,
		i.VelocidadX,
		i.VelocidadY,
		i.VelocidadZ,
		i.CampoX,
		i.CampoY,
		i.CampoZ,
		i.Temperatura,
		i.Humedad,
		i.Vibraciones,
	))
}

func randLetter(options []byte) byte {
	return options[rand.Intn(len(options))]
}

func randNumWithSign(n, d int) string {
	sign := ""
	numRand := strconv.Itoa(int(math.Pow10(n+d)) - rand.Intn(int(math.Pow10((n+d)-1))))
	if rand.Intn(2) == 1 {
		sign += "+"
	} else {
		sign += "-"
	}

	if d == 0 {
		return sign + numRand
	}
	return sign + numRand[:n] + "." + numRand[n:]
}

func checkErr(e error, t string) {
	if e != nil {
		log.Printf("ERROR: %s\n", t)
		log.Println(e)
	}
}
