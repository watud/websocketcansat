# Ejemplo del WebSocket (WS) para un dispositivo CanSat

Se puede encontrar el código del servidor, ademas de un pequeño ejemplo de como consumir el **WS**.

Para levantar los servicio se usa `Docker`, use los siguientes comandos:

```
$ cd <carpeta-del-repositorio>
$ docker-compose up -d --build
```

Puede entrar a ver el WS en [http://localhost:3030/](http://localhost:3030/). Otros comandos útiles de docker que puede usar son:

- `$ docker-compose logs -f` sirve para ver los logs del servidor.
- `$ docker-compose stop` detiene el contenedor y para el servidor.
- `$ docker-compose star` inicia el contenedor y el servidor.
- `$ docker-compose down` borra el contenedor y ya no seria posible usar el servidor.
- `$ docker-compose up -d` crear el contenedor e inicia el servidor.
